import {
    mdiAccountCircle,
    mdiMonitor,
    mdiGithub,
    mdiLock,
    mdiAlertCircle,
    mdiSquareEditOutline,
    mdiTable,
    mdiViewList,
    mdiTelevisionGuide,
    mdiResponsive,
    mdiPalette,
    mdiReact, mdiFaceMan, mdiTelevisionClassic
} from '@mdi/js'

export default [
  {
      route: 'dashboard',
    icon: mdiMonitor,
    label: 'Дашборд'
  },
  {
      route: 'specialities.index',
    label: 'Специализации',
    icon: mdiTable
  },
    {
        route: 'authors.index',
        label: 'Авторы',
        icon: mdiFaceMan
    },
    {
        route: 'products.index',
        label: 'Товары',
        icon: mdiTelevisionClassic
    }
]
