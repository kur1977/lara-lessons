<?php

namespace App\Faker;
use Faker\Provider\Base;
class ProductProvider extends Base
{
    protected static $names =  [
        'echo для чайников',
        'Логика в миграциях, как высшая степень просвещенности',
        'Laravel или Symphony? Какую таблетку выберешь ты?',
        'SQL запросы в циклах. Оказывается, тормоза придумали не трусы',
        'Vue и View - разные вещи. Открытия для тех, кто в танке',
        '1С-Битрикс. Программирование для мазохистов',
        'Не делай бекапы. Добавь новых красок в свою унылую жизнь',
        'Тонкий контроллер. Миф, или реальность',
        'Делаем лендинги на Laravel или мегаломания в дейстивии',
        'Observers для вуайеристов'
    ];
    public function product(): string
    {
        return static::randomElement(static::$names);
    }
}
