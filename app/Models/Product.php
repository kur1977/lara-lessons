<?php

namespace App\Models;

use App\Http\Filters\ProductFilter;
use App\Models\Traits\hasFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;

class Product extends Model
{
    use HasFactory;
    use SoftDeletes;
    use HasFilter;
    protected $guarded = false;
    protected $fillable = ['title', 'price', 'description', 'price_with_discount', 'author_id'];

    public function author(): BelongsTo
    {
        return $this->belongsTo(Author::class);
    }

    public function scopeAuthor(Builder $bulder, $title)
    {
        return $this->belongsTo(Author::class)->where('title','like', "'%$title%'");
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    public function orders(): BelongsToMany
    {
        return $this->belongsToMany(Order::class);
    }
    public function speciality()
    {
        return $this->author->speciality();
    }

    public function images(): hasMany
    {
        return $this->hasMany(Image::class);
    }

}
