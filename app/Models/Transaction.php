<?php

namespace App\Models;

use App\Models\Traits\HasFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use HasFactory;
    use SoftDeletes;
    use HasFilter;
    protected $guarded = false;

    const STATUS_CREATED = 1;
    const STATUS_SUCCESS = 2;
    const STATUS_FAILED = 3;

    const TYPE_DEBET = 1;
    const TYPE_CREDIT = 2;

    const STATUSES = [
        self::STATUS_CREATED => 'создан',
        self::STATUS_SUCCESS => 'оплачено',
        self::STATUS_FAILED => 'ошибка',
    ];

    const TYPES = [
        self::TYPE_DEBET => 'пополнение',
        self::TYPE_CREDIT => 'списание',
    ];

    protected $casts = [
        'date_transaction' => 'datetime',
    ];

    protected $appends = ['status_title'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function promocode(): BelongsTo
    {
        return $this->belongsTo(Promocode::class);
    }

    public function getStatusTitleAttribute()
    {
        return self::STATUSES[$this->status];
    }

}
