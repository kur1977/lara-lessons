<?php

namespace App\Models;

use App\Models\Traits\HasFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory;
    use SoftDeletes;
    use HasFilter;

    const STATUS_CREATED = 1;
    const STATUS_SUCCESS = 2;
    const STATUS_FAILED = 3;

    const STATUSES = [
        self::STATUS_CREATED => 'Создан',
        self::STATUS_SUCCESS => 'Оплачено',
        self::STATUS_FAILED => 'Ошибка',
    ];
    protected $guarded = false;
    protected $casts = [
        'order_date' => 'datetime',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function products()
    {
        return $this->user->orderProducts($this);
    }

    public function transaction(): hasOne
    {
        return $this->hasOne(Transaction::class);
    }

    public function scopeFailedOrders(Builder $builder, $from, $to)
    {
        return $builder->where('status', Order::STATUS_FAILED)
            ->where('total', '>', $from)
            ->where('total', '<', $to);
    }

    public function getTotalAmountAttribute()
    {
        return $this->user->orderProducts($this)->get()->sum(function ($product) {
            return $product->price * $product->pivot->quantity;
        });
    }
}
