<?php

namespace App\Models;

use App\Models\Traits\HasFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model
{
    use HasFactory;
    use SoftDeletes;
    use HasFilter;
    protected $guarded = false;
    protected $appends = ['fio'];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function getFioAttribute()
    {
        return implode(' ', [$this->first_name, $this->last_name]);
    }

}
