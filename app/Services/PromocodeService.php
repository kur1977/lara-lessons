<?php

namespace App\Services;

use App\Models\Promocode;

class PromocodeService
{
    public static function store(array $data): Promocode
    {
        return Promocode::create($data);
    }

    public static function update(Promocode $promocode, array $data): Promocode
    {
        $promocode->update($data);
        return $promocode->fresh();
    }
}
