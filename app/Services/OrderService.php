<?php

namespace App\Services;

use App\Models\Order;
use App\Models\Transaction;
use Illuminate\Support\Facades\DB;

class OrderService
{
    public static function store(array $data): Order
    {
        return Order::create($data);
    }

    public static function update(Order $order, array $data): Order
    {
        $order->update($data);
        return $order->fresh();
    }

    public static function storeByUser(): Order
    {
        try {
            DB::beginTransaction();
            $order = Order::create([
                'user_id'   => auth()->id(),
            ]);

            auth()->user()->productsInCart()->updateExistingPivot(auth()->user()->productsInCart()->pluck('products.id'),[
                'order_id'  => $order->id,
            ]);
            DB::commit();
        }catch(\Exception $exception){
            DB::rollBack();
        }
        return isset($order)?$order->fresh():[];
    }

    public static function updateByUser(Order $order, array $data): Order
    {
        try {
            DB::beginTransaction();
            auth()->user()->orderProducts($order)->syncWithoutDetaching([$data['product_id']=> [
                'quantity'  => $data['quantity'],
            ]]);
            DB::commit();
        }catch(\Exception $exception) {
            DB::rollBack();
        }
        return $order->fresh();
    }

    public static function destroyByUser(Order $order): void
    {
        try {
            DB::beginTransaction();
            auth()->user()->orderProducts($order)->sync([]);
            $order->delete();
            DB::commit();
        }catch(\Exception $exception) {
            DB::rollBack();
        }
    }


    public static function destroyProduct(Order $order, array $data): Order
    {
        auth()->user()->orderProducts($order)->detach($data['product_id']);
        return $order;
    }

    public  static function confirm(Order $order): Order
    {
        Transaction::create([
            'type'  => Transaction::TYPE_CREDIT,
            'amount'    => $order->totalAmount,
            'user_id'   => auth()->id(),
            'order_id'  => $order->id,
            'status'    => Transaction::STATUS_SUCCESS
        ]);

        $order->update([
            'status'    => Order::STATUS_SUCCESS
        ]);

        $order->user->profile->update([
            'balance' => $order->user->profile->balance - $order->totalAmount,
        ]);

        return $order->fresh();
    }
}


