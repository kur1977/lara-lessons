<?php

namespace App\Services;

use App\Models\Author;

class AuthorService
{
    public static function store(array $data): Author
    {
        return Author::create($data);
    }

    public static function update(Author $author, array $data): Author
    {
        $author->update($data);
        return $author->fresh();
    }
}
