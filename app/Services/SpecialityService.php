<?php

namespace App\Services;

use App\Models\Speciality;

class SpecialityService
{
    public static function store(array $data): Speciality
    {
        return Speciality::create($data);
    }

    public static function update(Speciality $speciality, array $data): Speciality
    {
        $speciality->update($data);
        return $speciality->fresh();
    }
}
