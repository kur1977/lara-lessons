<?php

namespace App\Services;

use App\Models\Order;
use App\Models\Promocode;
use App\Models\Transaction;
use Illuminate\Support\Facades\DB;

class TransactionService
{
    public static function store(array $data): Transaction
    {
        return Transaction::create($data);
    }

    public static function update(Transaction $transaction, array $data): Transaction
    {
        $transaction->update($data);
        return $transaction->fresh();
    }

    public static function storeByUser(array $data): Transaction
    {
        try{
            DB::beginTransaction();
            $transaction = Transaction::create($data);

            DB::commit();
        }catch(\Exception $exception){
            DB::rollBack();
        }

        return isset($transaction)?$transaction->fresh():[];
    }

    public static function updateFail(Transaction $transaction): Transaction
    {
        try{
            DB::beginTransaction();
            $transaction->update([
                'status'    => Transaction::STATUS_FAILED
            ]);
            DB::commit();
        }catch(\Exception $exception){
            DB::rollBack();
        }

        return $transaction->fresh();
    }

    public static function updateSuccess(Transaction $transaction): Transaction
    {

        try{
            DB::beginTransaction();

            $amount = $transaction->amount + ($transaction->promocode?$transaction->promocode->price:0);

            $transaction->user->profile->update([
                'balance' => $transaction->user->profile->balance + $amount,
            ]);

            $transaction->update([
                'status'    => Transaction::STATUS_SUCCESS
            ]);

            DB::commit();
        }catch(\Exception $exception){
            DB::rollBack();
        }

        return $transaction->fresh();
    }
}
