<?php

namespace App\Http\Resources\Promocode;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PromocodeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'code' => $this->code,
            'date_expire' => $this->date_expire,
            'price' => $this->price,
            'price_from' => $this->price_from,
            'user' => $this->user,
        ];
    }
}
