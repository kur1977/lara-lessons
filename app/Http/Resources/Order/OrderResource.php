<?php

namespace App\Http\Resources\Order;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'order_date' => $this->order_date,
            'status' => $this->status,
            'total' => $this->totalAmount,
            'user' => $this->user,
            'products'  => $this->products,
            'transaction'   => $this->transaction,
        ];
    }
}
