<?php

namespace App\Http\Resources\Author;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AuthorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'last_name' => $this->last_name,
            'sur_name' => $this->sur_name,
            'full_name' => $this->name.' '.$this->sur_name.' '. $this->last_name.' ',
            'speciality' => $this->speciality,
            'speciality_id' => $this->speciality_id
        ];
    }
}
