<?php

namespace App\Http\Resources\Transaction;

use App\Http\Resources\Order\OrderResource;
use App\Http\Resources\Promocode\PromocodeResource;
use App\Http\Resources\User\UserResource;
use App\Models\Promocode;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id'    => $this->id,
            'date_transaction' => $this->created_at->diffForHumans(),
            'amount' => $this->amount,
            'status' => $this->status,
            'user' => $this->user?UserResource::make($this->user):null,
            'order' => $this->order?OrderResource::make($this->order):null,
            'promocode' => $this->promocode_id?
                PromocodeResource::make($this->promocode)
                :null,
        ];
    }
}
