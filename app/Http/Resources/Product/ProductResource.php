<?php

namespace App\Http\Resources\Product;

use App\Http\Resources\Image\ImageResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Str;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            "id"    => $this->id,
            "title" => $this->title,
            "price" =>  $this->price,
            "description" => $this->description,
            "description_short" => Str::of($this->description)->words(5, ' ...'),
            "price_with_discount" => $this->price_with_discount,
            "author" => $this->author,
            "author_id" => $this->author_id,
            "created_at" =>  $this->created_at->toDateString(),
            "updated_at" =>  $this->updated_at->toDateString(),
            "images" => ImageResource::collection($this->images)->resolve(),
        ];
    }
}
