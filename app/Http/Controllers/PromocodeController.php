<?php

namespace App\Http\Controllers;

use App\Http\Requests\Promocode\UpdateUserRequest;
use App\Http\Resources\Promocode\PromocodeResource;
use App\Models\Promocode;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class PromocodeController extends Controller
{
    public function updateUser(UpdateUserRequest $request)
    {
        $data = $request->validated();

        $promocode = Promocode::where('code', $data['code'])->first();


        $promocode->update([
            'user_id' => auth()->user()->id
        ]);

        return PromocodeResource::make($promocode)->resolve();
    }

    public function destroy(Promocode $promocode)
    {
        $promocode->delete();
        return ResponseAlias::HTTP_OK;
    }
}
