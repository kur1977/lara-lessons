<?php

namespace App\Http\Controllers;

use App\Http\Requests\Admin\User\IndexRequest;
use App\Http\Requests\User\DestroyProductToCartRequest;
use App\Http\Requests\User\StoreProductToCartRequest;
use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateProductToCartRequest;
use App\Http\Resources\Product\ProductCartResource;
use App\Http\Resources\User\UserResource;
use App\Models\User;
use App\Services\UserService;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */

    //
    public function store(StoreRequest $request)
    {
        $data = $request->validated();
        $user = UserService::store($data);

        $user->access_token = JWTAuth::fromUser($user);

        return UserResource::make($user)->resolve();
    }

    public function update(StoreRequest $request)
    {
        $data = $request->validated();
        auth()->user()->update($data);

        return UserResource::make(auth()->user())->resolve();
    }

    public function destroy()
    {
        auth()->user()->delete();
        return ResponseAlias::HTTP_OK;
    }

    public function getProtuctsInCart(){
        return  ProductCartResource::collection(auth()->user()->productsInCart)->resolve();
    }

    public function storeProductToCart(StoreProductToCartRequest $request)
    {
        $data = $request->validated();
        auth()->user()->productsInCart()->syncWithoutDetaching([$data['product_id']=> $data]);
        return [ 'products_in_cart' => $this->getProtuctsInCart() ];
    }

    public function updateProductToCart(UpdateProductToCartRequest $request)
    {
        $data = $request->validated();
        auth()->user()->productsInCart()->updateExistingPivot($data['product_id'], $data);
        return [ 'products_in_cart' => $this->getProtuctsInCart() ];
    }

    public function destroyProductToCart(DestroyProductToCartRequest $request)
    {
        $data = $request->validated();
        auth()->user()->productsInCart()->detach($data['product_id']);
        return [ 'products_in_cart' => $this->getProtuctsInCart() ];
    }
}
