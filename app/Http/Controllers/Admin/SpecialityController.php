<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Speciality\IndexRequest;
use App\Http\Requests\Admin\Speciality\StoreRequest;
use App\Http\Requests\Admin\Speciality\UpdateRequest;
use App\Http\Resources\Speciality\SpecialityResource;
use App\Models\Product;
use App\Models\Speciality;
use App\Services\SpecialityService;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class SpecialityController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(IndexRequest $request)
    {
        $data = $request->validated();

        $specialities = Speciality::filter($data)->get();

        $specialities = SpecialityResource::collection($specialities)->resolve();

        return inertia('Admin/Speciality/Index', compact('specialities'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request)
    {
        $data = $request->validated();
        $speciality = SpecialityService::store($data);
        return SpecialityResource::make($speciality)->resolve();
    }

    /**
     * Display the specified resource.
     */
    public function show(Speciality $speciality)
    {
        return SpecialityResource::make($speciality)->resolve();
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, Speciality $speciality)
    {
        $data = $request->validated();
        $speciality = SpecialityService::update($speciality, $data);
        return SpecialityResource::make($speciality)->resolve();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Speciality $speciality)
    {
        $speciality->delete();
        return ResponseAlias::HTTP_OK;
    }

    public function create()
    {
        return inertia('Admin/Speciality/Create');
    }

    public function edit(Speciality $speciality)
    {
        $speciality = SpecialityResource::make($speciality)->resolve();
        return inertia('Admin/Speciality/Edit', compact('speciality'));
    }
}
