<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Image;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class ImageController extends Controller
{
    public function destroy(Image $image)
    {
        $image->delete();
        return ResponseAlias::HTTP_OK;
    }
}
