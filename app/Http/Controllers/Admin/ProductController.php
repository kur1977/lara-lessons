<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Product\IndexRequest;
use App\Http\Requests\Admin\Product\StoreRequest;
use App\Http\Requests\Admin\Product\UpdateRequest;
use App\Http\Resources\Author\AuthorSelectResource;
use App\Http\Resources\Product\ProductResource;
use App\Models\Author;
use App\Models\Image;
use App\Models\Product;
use App\Services\ProductService;
use DB;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(IndexRequest $request)
    {
        $data = $request->validated();

        $products = Product::filter($data)->paginate(5);

        $products =  ProductResource::collection($products);

        $prices = DB::table('products')
        ->select(\DB::raw('MIN(price) AS minPrice, MAX(price) AS maxPrice, MIN(price_with_discount) AS minPriceWithDiscount, MAX(price_with_discount) AS maxPriceWithDiscount'))
        ->whereNull('deleted_at')->first();



        if($request->wantsJson()){
            return $products;
        }
        return inertia('Admin/Product/Index', compact('products', 'prices'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request)
    {
        $data = $request->validationData();

        $product = ProductService::store($data['product']);

        foreach($data['images'] as $image){
            $path =  Storage::disk('public')->put('images', $image);
            Image::create([
                'product_id'    => $product->id,
                'url'   => $path
            ]);
        }

        return ProductResource::make($product)->resolve();
    }

    /**
     * Display the specified resource.
     */
    public function show(Product $product)
    {
        return ProductResource::make($product)->resolve();
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, Product $product)
    {
        $data = $request->validationData();



        $product = ProductService::update($product, $data['product']);

        if($data['images']){
            $product->images()->delete();
        }

        foreach($data['images'] as $image){
            $path =  Storage::disk('public')->put('images', $image);
            Image::create([
                'product_id'    => $product->id,
                'url'   => $path
            ]);
        }

        return ProductResource::make($product)->resolve();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $product)
    {
        $product->images()->delete();
        $product->delete();
        return ResponseAlias::HTTP_OK;
    }

    public function create()
    {
        $authors = Author::orderBy('last_name', 'ASC')->get();
        $authors = AuthorSelectResource::collection($authors)->resolve();
        return inertia('Admin/Product/Create', compact('authors'));
    }

    public function edit(Product $product)
    {
        $product = ProductResource::make($product)->resolve();

        $authors = Author::orderBy('last_name', 'ASC')->get();
        $authors = AuthorSelectResource::collection($authors)->resolve();
        return inertia('Admin/Product/Edit', compact('product', 'authors'));
    }
}
