<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Author\IndexRequest;
use App\Http\Requests\Admin\Author\StoreRequest;
use App\Http\Requests\Admin\Author\UpdateRequest;
use App\Http\Resources\Author\AuthorResource;
use App\Http\Resources\Speciality\SpecialitySelectResource;
use App\Models\Author;
use App\Models\Speciality;
use App\Services\AuthorService;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(IndexRequest $request)
    {
        $data = $request->validated();

        $authors = Author::filter($data)->get();

        $authors = AuthorResource::collection($authors)->resolve();

        if($request->wantsJson()){
            return $authors;
        }

        return inertia('Admin/Author/Index', compact('authors'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request)
    {
        $data = $request->validated();
        $author = AuthorService::store($data);
        return AuthorResource::make($author)->resolve();
    }

    /**
     * Display the specified resource.
     */
    public function show(Author $author)
    {
        return AuthorResource::make($author)->resolve();
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, Author $author)
    {
        $data = $request->validated();
        $author = AuthorService::update($author, $data);
        return AuthorResource::make($author)->resolve();
    }
    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Author $author)
    {
        $author->delete();
        return ResponseAlias::HTTP_OK;
    }

    public function create()
    {
        $specialities = Speciality::orderBy('title', 'ASC')->get();
        $specialities = SpecialitySelectResource::collection($specialities)->resolve();
        return inertia('Admin/Author/Create', compact('specialities'));
    }

    public function edit(Author $author)
    {
        $author = AuthorResource::make($author)->resolve();
        $specialities = Speciality::orderBy('title', 'ASC')->get();
        $specialities = SpecialitySelectResource::collection($specialities)->resolve();
        return inertia('Admin/Author/Edit', compact('author', 'specialities'));
    }
}
