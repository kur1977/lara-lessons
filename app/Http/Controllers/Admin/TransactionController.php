<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Transaction\IndexRequest;
use App\Http\Requests\Admin\Transaction\StoreRequest;
use App\Http\Requests\Admin\Transaction\UpdateRequest;
use App\Http\Resources\Transaction\TransactionResource;
use App\Models\Transaction;
use App\Services\TransactionService;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(IndexRequest $request)
    {
        $data = $request->validated();

        $transactions = Transaction::filter($data)->get();

        return TransactionResource::collection($transactions)->resolve();
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request)
    {
        $data = $request->validated();
        $transaction = TransactionService::store($data);
        return TransactionResource::make($transaction);
    }

    /**
     * Display the specified resource.
     */
    public function show(Transaction $transaction)
    {
        return TransactionResource::make($transaction)->resolve();
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, Transaction $transaction)
    {
        $data = $request->validated();
        $transaction = TransactionService::update($transaction, $data);
        return TransactionResource::make($transaction);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Transaction $transaction)
    {
        $transaction->delete();
        return ResponseAlias::HTTP_OK;
    }

    public function create()
    {

    }

    public function edit(Transaction $transaction)
    {

    }

    public function chart(){
        $transactions = Transaction::latest()->get()->groupBy(function($item, $key){
            return $item->created_at->format('Y-m');
        });

        //$data = [];
        $data = $transactions->map(function($el) {
            return $el->sum('amount');
        })->toArray();


        $range = CarbonPeriod::create(Carbon::now()->subYear()->toDateString(), '1 month', Carbon::now()->toDateString());
        $data2 = [];
        foreach($range as $dt){
            $data2[$dt->format("Y-m")] = 0;
        }

        $data = array_merge($data2, $data );

        return [
            "labels"=> array_keys($data),
            "data"  => array_values($data),
            ];
    }
}
