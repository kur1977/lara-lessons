<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Promocode\IndexRequest;
use App\Http\Requests\Admin\Promocode\StoreRequest;
use App\Http\Requests\Admin\Promocode\UpdateRequest;
use App\Http\Resources\Promocode\PromocodeResource;
use App\Models\Promocode;
use App\Services\PromocodeService;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class PromocodeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(IndexRequest $request)
    {
        $data = $request->validated();

        $promocodes = Promocode::filter($data)->get();

        return PromocodeResource::collection($promocodes)->resolve();
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request)
    {
        $data = $request->validated();
        $promocode = PromocodeService::store($data);
        return PromocodeResource::make($promocode);
    }

    /**
     * Display the specified resource.
     */
    public function show(Promocode $promocode)
    {
        return PromocodeResource::make($promocode)->resolve();
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, Promocode $promocode)
    {
        $data = $request->validated();
        $promocode = PromocodeService::update($promocode, $data);
        return PromocodeResource::make($promocode);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Promocode $promocode)
    {
        $promocode->delete();
        return ResponseAlias::HTTP_OK;
    }

    public function create()
    {

    }

    public function edit(Promocode $promocode)
    {

    }
}
