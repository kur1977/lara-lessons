<?php

namespace App\Http\Controllers;


use App\Http\Requests\Transaction\StoreRequest;
use App\Http\Requests\Transaction\UpdateRequest;
use App\Http\Resources\Transaction\TransactionResource;
use App\Http\Resources\User\UserResource;
use App\Models\Order;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use App\Services\TransactionService;
use Inertia\Inertia;

class TransactionController extends Controller
{
    public function index()
    {
        return TransactionResource::collection(Transaction::all())->resolve();
    }

    public function show(Transaction $transaction)
    {

        return TransactionResource::make($transaction)->resolve();
    }

    public function create()
    {
        $user = UserResource::make(User::find(auth()->id()))->resolve();
        return inertia('Transaction/create', compact('user'));
    }

    public function store(StoreRequest $request)
    {

        $data = $request->validationData();

        $transaction = TransactionService::storeByUser($data);
        $transaction = $transaction->fresh();


        return Inertia::location("http://lara-lessons-pay.loc/transaction?transaction_idx=$transaction->id&amount=$transaction->amount");
        //return TransactionResource::make($transaction)->resolve();
    }

    public function updateFail(Transaction $transaction, UpdateRequest $request)
    {
        TransactionService::updateFail($transaction);
        return TransactionResource::make($transaction->fresh())->resolve();
    }

    public function updateSuccess(Transaction $transaction, UpdateRequest $request)
    {

        TransactionService::updateSuccess($transaction);
        return TransactionResource::make($transaction->fresh())->resolve();
    }
}
