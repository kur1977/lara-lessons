<?php

namespace App\Http\Controllers;

use App\Http\Requests\Order\ConfirmRequest;
use App\Http\Requests\Order\DestroyProductRequest;
use App\Http\Requests\Order\StoreRequest;
use App\Http\Requests\Order\UpdateProductRequest;
use App\Http\Requests\Order\UpdateStatusRequest;
use App\Http\Resources\Order\OrderResource;
use App\Models\Order;
use App\Models\Transaction;
use App\Services\OrderService;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class OrderController extends Controller
{
    public function index()
    {
        return OrderResource::collection(auth()->user()->orders)->resolve();
    }

    public function store(StoreRequest $request)
    {
        //$data = $request->validated();
        $order = OrderService::storeByUser();
        return OrderResource::make($order->fresh())->resolve();
    }

    public function show(Order $order)
    {
        return OrderResource::make($order)->resolve();
    }

    public function confirm(Order $order, ConfirmRequest $request)
    {
        $order = OrderService::confirm($order);
        return OrderResource::make($order)->resolve();
    }

    public function updateProduct(Order $order, UpdateProductRequest $request)
    {
        $data = $request->validated();
        $order = OrderService::updateByUser($order, $data);
        return OrderResource::make($order)->resolve();
    }

    public function destroyProduct(Order $order, DestroyProductRequest $request)
    {
        $data = $request->validated();
       $order = OrderService::destroyProduct($order, $data);

        return OrderResource::make($order)->resolve();
    }

    public function destroy(Order $order)
    {
        OrderService::destroyByUser($order);
        return ResponseAlias::HTTP_OK;
    }
}
