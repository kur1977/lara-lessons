<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class UserFilter extends AbstractFilter
{
    protected $keys = [
        'name',
        'email',
        'created_at_from',
        'created_at_to',
    ];

    protected function name(Builder $builder, $value){
        $builder->where('name', 'like',"%$value%");
    }


    protected function email(Builder $builder, $value){
        $builder->where('email', $value);
    }

    protected function createdAtFrom(Builder $builder, $value){
        $builder->where('created_at', '>=',$value);
    }

    protected function createdAtTo(Builder $builder, $value){
        $builder->where('created_at', '<=',$value);
    }

}
