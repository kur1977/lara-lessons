<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class OrderFilter extends AbstractFilter
{
    protected $keys = [
        'user',
        'status',
        'created_at_from',
        'created_at_to',
    ];

    protected function user(Builder $builder, $value){
        $builder->whereRelation('user', 'name','like', "%$value%");
    }

    protected function status(Builder $builder, $value){
        $builder->where('status', $value);
    }


    protected function createdAtFrom(Builder $builder, $value){
        $builder->where('created_at', '>=',$value);
    }

    protected function createdAtTo(Builder $builder, $value){
        $builder->where('created_at', '<=',$value);
    }

}
