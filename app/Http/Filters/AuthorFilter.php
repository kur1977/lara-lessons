<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class AuthorFilter extends AbstractFilter
{
    protected $keys = [
        'name',
        'last_name',
        'sur_name',
        'created_at_from',
        'created_at_to',
        'speciality',
    ];

    protected function name(Builder $builder, $value){

        $builder->where('name', 'like',"%$value%");
    }

    protected function lastName(Builder $builder, $value){

        $builder->where('last_name', 'like',"%$value%");
    }

    protected function surName(Builder $builder, $value){

        $builder->where('sur_name', 'like',"%$value%");
    }

    protected function createdAtFrom(Builder $builder, $value){
        $builder->where('created_at', '>=',$value);
    }

    protected function createdAtTo(Builder $builder, $value){
        $builder->where('created_at', '<=',$value);
    }

    protected function speciality(Builder $builder, $value){
        $builder->whereRelation('speciality', 'title','like', "%$value%");
    }
}
