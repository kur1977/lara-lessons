<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class ProfileFilter extends AbstractFilter
{
    protected $keys = [
        'first_name',
        'last_name',
        'gender',
        'birthday_from',
        'birthday_to',
        'address',
        'work_place',
        'login',
        'balance_from',
        'balance_to',
        'created_at_from',
        'created_at_to',
        'user',
    ];

    protected function firstName(Builder $builder, $value){

        $builder->where('first_name', 'like',"%$value%");
    }

    protected function lastName(Builder $builder, $value){

        $builder->where('last_name', 'like',"%$value%");
    }

    protected function gender(Builder $builder, $value){

        $builder->where('gender', 'like',"%$value%");
    }

    protected function birthdayFrom(Builder $builder, $value){
        $builder->where('birthday', '>=',$value);
    }

    protected function birthdayTo(Builder $builder, $value){
        $builder->where('birthday', '<=',$value);
    }

    protected function address(Builder $builder, $value){

        $builder->where('address', 'like',"%$value%");
    }

    protected function workPlace(Builder $builder, $value){

        $builder->where('work_place', 'like',"%$value%");
    }

    protected function login(Builder $builder, $value){

        $builder->where('login', 'like',"%$value%");
    }

    protected function balanceFrom(Builder $builder, $value){
        $builder->where('balance', '>=',$value);
    }

    protected function balanceTo(Builder $builder, $value){
        $builder->where('balance', '<=',$value);
    }
    protected function createdAtFrom(Builder $builder, $value){
        $builder->where('created_at', '>=',$value);
    }

    protected function createdAtTo(Builder $builder, $value){
        $builder->where('created_at', '<=',$value);
    }

    protected function user(Builder $builder, $value){
        $builder->whereRelation('user', 'name','like', "%$value%");
    }
}
