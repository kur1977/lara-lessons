<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class SpecialityFilter extends AbstractFilter
{
    protected $keys = [
        'title',
        'created_at_from',
        'created_at_to',
    ];

    protected function title(Builder $builder, $value){

        $builder->where('title', 'like',"%$value%");
    }

    protected function createdAtFrom(Builder $builder, $value){
        $builder->where('created_at', '>=',$value);
    }

    protected function createdAtTo(Builder $builder, $value){
        $builder->where('created_at', '<=',$value);
    }
}
