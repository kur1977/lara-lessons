<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class TransactionFilter extends AbstractFilter
{
    protected $keys = [
        'amount_from',
        'amount_to',
        'payment_system',
        'type',
        'status',
        'created_at_from',
        'created_at_to',
        'user_id',
        'order_id',
    ];

    protected function amountFrom(Builder $builder, $value){
        $builder->where('amount', '>=',$value);
    }

    protected function amountTo(Builder $builder, $value){
        $builder->where('amount', '<=',$value);
    }

    protected function paymentSystem(Builder $builder, $value){

        $builder->where('payment_system', 'like',"%$value%");
    }

    protected function type(Builder $builder, $value){
        $builder->where('type', $value);
    }

    protected function status(Builder $builder, $value){
        $builder->where('status', $value);
    }

    protected function createdAtFrom(Builder $builder, $value){
        $builder->where('created_at', '>=',$value);
    }

    protected function createdAtTo(Builder $builder, $value){
        $builder->where('created_at', '<=',$value);
    }

    protected function userId(Builder $builder, $value){
        $builder->where('user_id', $value);
    }

    protected function orderId(Builder $builder, $value){
        $builder->where('order_id', $value);
    }
}
