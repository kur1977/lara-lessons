<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class PromocodeFilter extends AbstractFilter
{
    protected $keys = [
        'code',
        'date_expire_from',
        'date_expire_from_to',
        'price_from',
        'price_to',
        'price_from_from',
        'price_from_to',
        'created_at_from',
        'created_at_to',
        'user',
    ];

    protected function code(Builder $builder, $value){

        $builder->where('code', 'like',"%$value%");
    }

    protected function dateExpireFrom(Builder $builder, $value){
        $builder->where('date_expire', '>=',$value);
    }

    protected function dateExpireTo(Builder $builder, $value){
        $builder->where('date_expire', '<=',$value);
    }

    protected function priceFrom(Builder $builder, $value){
        $builder->where('price', '>=',$value);
    }

    protected function priceTo(Builder $builder, $value){
        $builder->where('price', '<=',$value);
    }

    protected function priceFromFrom(Builder $builder, $value){
        $builder->where('price_from', '>=',$value);
    }

    protected function priceFromTo(Builder $builder, $value){
        $builder->where('price_from', '<=',$value);
    }

    protected function createdAtFrom(Builder $builder, $value){
        $builder->where('created_at', '>=',$value);
    }

    protected function createdAtTo(Builder $builder, $value){
        $builder->where('created_at', '<=',$value);
    }

    protected function user(Builder $builder, $value){
        $builder->whereRelation('user', 'name','like', "%$value%");
    }
}
