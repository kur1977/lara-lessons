<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class ProductFilter extends AbstractFilter
{
    protected $keys = [
        'title',
        'price_from',
        'price_to',
        'description',
        'created_at_from',
        'created_at_to',
        'author',
    ];

    protected function title(Builder $builder, $value){

        $builder->where('title', 'like',"%$value%");
    }

    protected function priceFrom(Builder $builder, $value){
        $builder->where('price', '>=',$value);
    }

    protected function priceTo(Builder $builder, $value){
        $builder->where('price', '<=',$value);
    }

    protected function description(Builder $builder, $value){
        $builder->where('description', 'like',"%$value%");
    }

    protected function createdAtFrom(Builder $builder, $value){
        $builder->where('created_at', '>=',$value);
    }

    protected function createdAtTo(Builder $builder, $value){
        $builder->where('created_at', '<=',$value);
    }

    protected function author(Builder $builder, $value){
        $builder->whereRelation('author', 'name','like', "%$value%");
    }
}
