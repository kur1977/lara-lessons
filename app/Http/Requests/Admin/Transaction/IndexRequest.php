<?php

namespace App\Http\Requests\Admin\Transaction;

use App\Models\Transaction;
use Illuminate\Foundation\Http\FormRequest;

class IndexRequest extends FormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'amount_from' => 'nullable|numeric',
            'amount_to' => 'nullable|numeric',
            'payment_system' => 'nullable|string',
            'type' => 'nullable|integer|in:'.implode(',', array_keys(Transaction::TYPES)),
            'status' => 'nullable|integer|in:'.implode(',', array_keys(Transaction::STATUSES)),
            'created_at_from' => 'nullable|date_format:Y-m-d',
            'created_at_to' => 'nullable|date_format:Y-m-d',
            'user_id' => 'nullable|integer',
            'order_id' => 'nullable|integer',
        ];
    }
}


