<?php

namespace App\Http\Requests\Admin\Profile;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'first_name' => 'nullable|string',
            'second_name' => 'nullable|string',
            'third_name' => 'nullable|string',
            'gender' => 'nullable|integer',
            'balance' => 'nullable|numeric',
            'phone' => 'nullable|string',
            'medical_card' => 'nullable|string',
            'birthed_at' => 'nullable|date_format:Y-m-d',
            'user_id' => 'required|integer',
        ];
    }
}
