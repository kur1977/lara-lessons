<?php

namespace App\Http\Requests\Admin\Profile;

use Illuminate\Foundation\Http\FormRequest;

class IndexRequest extends FormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'first_name' => 'nullable|string',
            'last_name' => 'nullable|string',
            'gender' => 'nullable|string',
            'birthday_from' => 'nullable|date_format:Y-m-d',
            'birthday_to' => 'nullable|date_format:Y-m-d',
            'address' => 'nullable|string',
            'work_place' => 'nullable|string',
            'login' => 'nullable|string',
            'balance_from' => 'nullable|numeric',
            'balance_to' => 'nullable|numeric',
            'created_at_from' => 'nullable|date_format:Y-m-d',
            'created_at_to' => 'nullable|date_format:Y-m-d',
            'user' => 'nullable|string',
        ];
    }
}


