<?php

namespace App\Http\Requests\Admin\Promocode;

use Illuminate\Foundation\Http\FormRequest;

class IndexRequest extends FormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'code' => 'nullable|string',
            'date_expire_from' => 'nullable|date_format:Y-m-d',
            'date_expire_from_to' => 'nullable|date_format:Y-m-d',
            'price_from'        => 'nullable|numeric',
            'price_to'        => 'nullable|numeric',
            'price_from_from'        => 'nullable|numeric',
            'price_from_to'        => 'nullable|numeric',
            'created_at_from' => 'nullable|date_format:Y-m-d',
            'created_at_to' => 'nullable|date_format:Y-m-d',
            'user' => 'nullable|string',
        ];
    }
}


