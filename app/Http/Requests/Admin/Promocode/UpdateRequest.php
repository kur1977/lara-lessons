<?php

namespace App\Http\Requests\Admin\Promocode;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'code' => 'required|string',
            'date_expire' => 'required|date',
            'price' => 'required|numeric',
            'price_from' => 'required|numeric',
            'user_id' => 'nullable|integer|exists:users,id',
        ];
    }
}
