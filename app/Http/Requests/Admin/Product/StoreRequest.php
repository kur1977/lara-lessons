<?php

namespace App\Http\Requests\Admin\Product;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'product.title' => 'required|string',
            'product.price' => 'required|numeric',
            'product.description' => 'nullable|string',
            'product.category' => 'nullable|string',
            'product.author_id' => 'nullable|integer|exists:authors,id',
            'product.price_with_discount' => 'nullable|numeric',
            'images' => 'nullable|array',
            'images.*' => 'nullable|file',
        ];
    }

    protected function passedValidation()
    {
        return $this->replace($this->except(['product.description_short', 'image']))->merge([
            'images' => $this->images??[]
        ]);
    }
}
