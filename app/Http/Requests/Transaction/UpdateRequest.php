<?php

namespace App\Http\Requests\Transaction;

use App\Models\Transaction;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'not_success'  => 'required|bool|in:1'
        ];
    }

    public function prepareForValidation()
    {

        return $this->merge([
            'not_success'   => $this->route('transaction')->status!=Transaction::STATUS_SUCCESS?1:0,
        ]);
    }

    public function messages()
    {
        return [
            'not_success' => 'Транзакция уже завершена'
        ];
    }
}
