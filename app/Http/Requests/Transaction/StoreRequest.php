<?php

namespace App\Http\Requests\Transaction;

use App\Http\Resources\Promocode\PromocodeResource;
use App\Models\Promocode;
use App\Models\Transaction;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'user_id'   => 'required|exists:users,id',
            'amount'    => 'required|integer',
            'type'      => 'nullable|integer|in:'.implode(',', array_keys(Transaction::TYPES)),
            'promocode' => 'nullable|string'
        ];
    }

    public function prepareForValidation()
    {
        return $this->merge([
            'user_id' => auth()->id(),
        ]);
    }

    public function passedValidation()
    {
        $promocode = auth()->user()->promocodes()->where('code', $this->promocode)
            ->where('date_expire', '>', now()->toDateString())
            ->where('price_from', '<', $this->amount)
            ->first();

        $this->offsetUnset('promocode');

        return $this->merge([
            'promocode_id' => $promocode ? $promocode->id : null,
        ])->except(['promocode']);
    }
}
