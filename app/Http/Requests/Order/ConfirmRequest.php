<?php

namespace App\Http\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;

class ConfirmRequest extends FormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'has_transaction'  => 'required|bool|in:1',
            'has_money'   => 'required|bool|in:1',
        ];
    }

    public function prepareForValidation()
    {
        return $this->merge([
            'has_transaction'   => is_null($this->route('order')->transaction),
            'has_money'   => $this->route('order')->user->profile->balance >= $this->route('order')->totalAmount?1:0,
        ]);
    }

    public function messages()
    {
        return [
            'has_transaction' => 'У заказа уже есть транзакция',
            'has_money' => 'Недостаточно средств на балансе',
        ];
    }
}
