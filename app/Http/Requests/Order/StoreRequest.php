<?php

namespace App\Http\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'cart_has_products' => 'required|boolean|in:1'
        ];
    }

    public function prepareForValidation()
    {
        return $this->merge([
            'cart_has_products' => auth()->user()->productsInCart()->count()>0,
        ]);
    }

    public function messages()
    {
        return [
            'cart_has_products' => 'Корзина пуста'
        ];
    }
}
