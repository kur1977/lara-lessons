<?php

namespace App\Http\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'product_id'  => 'required|integer|exists:product_user,product_id',
            'quantity'  => 'required|integer|min:1',
            'status_is_new' => 'required|boolean|in:1',
        ];
    }

    public function prepareForValidation()
    {

        return $this->merge([
            'status_is_new' => $this->route('order')->status == 1,
        ]);
    }

    public function messages()
    {
        return [
            'status_is_new' => 'Нельзя отредактировать не новый заказ',
        ];
    }
}
