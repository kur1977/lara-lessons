<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('products', function (Blueprint $table) {
            $table->foreignId('author_id')->index()->nullable()->constrained('authors');
            $table->text('description')->nullable()->change();
            $table->decimal('price_with_discount')->nullable()->change();
            $table->dropColumn('category');
            $table->dropColumn('author');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('author');
            $table->string('category');
            $table->decimal('price_with_discount')->nullable()->change();
            $table->text('description')->nullable()->change();
            $table->dropConstrainedForeignId('author_id');
        });
    }
};
