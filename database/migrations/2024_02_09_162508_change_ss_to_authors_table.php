<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('authors', function (Blueprint $table) {
            $table->foreignId('speciality_id')->index()->nullable()->constrained('specialities');
            $table->dropColumn('speciality');
            $table->string('sur_name')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('authors', function (Blueprint $table) {
            $table->string('sur_name')->nullable()->change();
            $table->string('speciality');
            $table->dropConstrainedForeignId('speciality_id');
        });
    }
};
