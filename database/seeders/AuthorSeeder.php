<?php

namespace Database\Seeders;

use App\Models\Author;
use App\Models\Speciality;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $authors = Author::factory(10)->create();

        $speciality_ids = Speciality::all()->pluck('id');
        $speciality_ids->push(null);

        $authors->map( fn($author) => $author->update(
            ['speciality_id' => $speciality_ids->random(1)[0]]
        ));

    }
}
