<?php

namespace Database\Seeders;

use App\Models\Promocode;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PromocodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $users = User::doesntHave('roles')
            ->orWhereRelation('roles', 'title', '!=', 'admin')
            ->inRandomOrder()
            ->limit(5)
            ->get();

        $users->map(function($user){
            Promocode::create([
                'code' => fake()->word,
                'date_expire' => fake()->dateTimeBetween(now()->toDateString(), '2025-01-01'),
                'price' => fake()->numberBetween(10, 1000),
                'price_from' => fake()->numberBetween(1000, 10000),
                'user_id' => $user->id,
            ]);
        });
    }
}
