<?php

namespace Database\Seeders;

use App\Models\Order;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $users = User::with('promocodes')->doesntHave('roles')
            ->orWhereRelation('roles', 'title', '!=', 'admin')
            ->inRandomOrder()
            ->limit(5)->get();

         $users->map(fn ($user) => Transaction::create([
             'amount' => fake()->numberBetween(10, 100),
             'type' => Transaction::TYPE_DEBET,
             'status' => rand(1,3),
             'user_id' => $user->id,
             'promocode_id' => !is_null($user->promocodes->first())?$user->promocodes->first()->id:null,
         ]));

        $orders = Order::all();

        $orders->map(fn ($order) => Transaction::create([
            'type'  => Transaction::TYPE_CREDIT,
            'amount'    => $order->totalAmount,
            'user_id'   => $order->user_id,
            'order_id'  => $order->id,
            'status'    => array_keys(Transaction::STATUSES)[array_rand(array_keys(Transaction::STATUSES))]
        ]));
    }
}
