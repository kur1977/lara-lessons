<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            RoleSeeder::class,
            UserSeeder::class,
            ProfileSeeder::class,
            PromocodeSeeder::class,
            SpecialitySeeder::class,
            AuthorSeeder::class,
            ProductSeeder::class,
            ProductUserSeeder::class,
            OrderSeeder::class,
            TransactionSeeder::class,
        ]);
    }
}
