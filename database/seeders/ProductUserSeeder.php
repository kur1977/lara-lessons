<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $products = Product::inRandomOrder()->limit(5)->get();
        $user_ids = User::doesntHave('roles')
            ->orWhereRelation('roles', 'title', '!=', 'admin')
            ->inRandomOrder()
            ->limit(5)->get()->pluck('id');

        $products->map(function($product) use ($user_ids){
            $user_data = [];
            $user_ids->random(2)->map(function($user_id) use(&$user_data){
                $user_data[$user_id] = ['quantity'=>fake()->numberBetween(1, 10)];
            });

            $product->users()->sync($user_data);
        });
    }
}
