<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $user = [
            'name' => 'Админ',
            'email' => 'admin@lara-lessons.loc',
            'email_verified_at' => now()->toDateString(),
            'password' => \Hash::make('11111'),
        ];

        $user = User::firstOrCreate(['email' => $user['email']], $user);
        $admin_id = Role::where('title', 'admin')->first()->id;

        $user->roles()->sync($admin_id);

        User::factory(10)->create();
    }
}
