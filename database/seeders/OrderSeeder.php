<?php

namespace Database\Seeders;

use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $users = User::with('products')->whereHas('products')->limit(3)->get();

        $users->map( function($user) {
            $order = Order::create(['user_id' => $user->id]);
            $user->products->map(fn($product) =>
                $user->productsInCart()->updateExistingPivot($product->pivot->product_id, ['order_id' => $order->id])
            );
        });

    }
}
