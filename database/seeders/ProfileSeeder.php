<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $users = User::all();
        $users->map(function($user){
            $genders = ['male', 'female'];
            $gender = $genders[array_rand($genders, 1)];
            $user->profile->update( [
                'last_name' => fake()->lastName($gender),
                'gender' => $gender,
                'birthday' => fake()->date('Y-m-d'),
                'address' => fake()->address,
                'work_place' => fake()->address,
                'login' => fake()->userName,
                'balance' => fake()->numberBetween(0, 1000),
                'first_name' => fake()->firstName($gender),
            ]);
        });
    }
}
