<?php

namespace Database\Seeders;

use App\Models\Author;
use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $titles = [
            'echo для чайников',
            'Логика в миграциях, как высшая степень просвещенности',
            'Laravel или Symphony? Какую таблетку выберешь ты?',
            'SQL запросы в циклах. Оказывается, тормоза придумали не трусы',
            'Vue и View - разные вещи. Открытия для тех, кто в танке',
            '1С-Битрикс. Программирование для мазохистов',
            'Не делай бекапы. Добавь новых красок в свою унылую жизнь',
            'Тонкий контроллер. Миф, или реальность',
            'Делаем лендинги на Laravel или мегаломания в дейстивии',
            'Observers для вуайеристов'
        ];
        $author_ids = Author::all()->pluck('id');
        $author_ids->push(null);

        $products = Product::factory()->count(10)->sequence(fn (Sequence $sequence) => [
            'title' => $titles[$sequence->index],
            'author_id' => $author_ids->random(1)[0]
        ])->create();
    }
}
