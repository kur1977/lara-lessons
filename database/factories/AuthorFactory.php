<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Author>
 */
class AuthorFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $genders = ['male', 'female'];
        $gender = $genders[array_rand($genders, 1)];

        return [
            'name' => fake()->firstName($gender),
            'last_name' => fake()->lastName($gender),
            'sur_name' => fake()->middleName($gender),
        ];
    }
}
