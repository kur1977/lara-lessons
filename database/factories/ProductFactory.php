<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $price = fake()->numberBetween(100, 1000);
        return [
           // 'title' => $this->faker->product,
            'price' => $price,
            'description' => fake()->text,
            'price_with_discount' => $price - (int)fake()->numberBetween(0, $price/10),
        ];
    }
}
