<?php

use App\Http\Controllers\Admin\AuthorController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\PromocodeController;
use App\Http\Controllers\Admin\SpecialityController;
use App\Http\Controllers\Admin\TransactionController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('vue', [\App\Http\Controllers\VueController::class, 'index']);

Route::group(['prefix'=>'admin'], function(){
    Route::resource('authors', AuthorController::class);
    Route::resource('orders', OrderController::class);
    Route::resource('products', ProductController::class);
    Route::resource('profiles', ProfileController::class);
    Route::resource('promocodes', PromocodeController::class);
    Route::resource('specialities', SpecialityController::class);
    Route::resource('transactions', TransactionController::class);
    Route::resource('users', UserController::class);
    Route::delete('images/{image}', [\App\Http\Controllers\Admin\ImageController::class, 'destroy']);

    Route::get('dashboard', function () {
        return Inertia::render('HomeView');
    })->name('dashboard');
    //->middleware(['auth', 'verified'])->name('dashboard');
});


Route::group(['middleware'=>'auth'], function() {
    Route::get('products', [\App\Http\Controllers\ProductController::class, 'index']);

    Route::get('user/products_in_cart', [\App\Http\Controllers\UserController::class, 'getProtuctsInCart']);
    Route::post('users/product_to_cart', [\App\Http\Controllers\UserController::class, 'storeProductToCart']);
    Route::patch('users/product_to_cart', [\App\Http\Controllers\UserController::class, 'updateProductToCart']);
    Route::delete('users/product_to_cart',  [\App\Http\Controllers\UserController::class, 'destroyProductToCart']);

    Route::post('orders', [\App\Http\Controllers\OrderController::class, 'store']);
    Route::get('transactions/create', [\App\Http\Controllers\TransactionController::class, 'create']);
    Route::post('transactions/store', [\App\Http\Controllers\TransactionController::class, 'store'])->name('transactions.store');
});

Route::get('transactions', [\App\Http\Controllers\TransactionController::class, 'index']);

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});



Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
