<?php

use App\Http\Controllers\Admin\AuthorController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\PromocodeController;
use App\Http\Controllers\Admin\SpecialityController;
use App\Http\Controllers\Admin\TransactionController;
use App\Http\Controllers\Admin\UserController as AdminUserController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => 'api'], function () {
    Route::post('login', [AuthController::class, 'login']);


    Route::post('users', [UserController::class, 'store']);
});



Route::group(['middleware' => ['jwt.auth', 'auth.admin'], 'prefix' => 'admin'], function () {

    Route::apiResource('authors', AuthorController::class);
    Route::apiResource('orders', OrderController::class);
    Route::apiResource('products', ProductController::class);
    Route::apiResource('profiles', ProfileController::class);
    Route::apiResource('promocodes', PromocodeController::class);
    Route::apiResource('specialities', SpecialityController::class);
    Route::apiResource('transactions', TransactionController::class);
    Route::apiResource('users', AdminUserController::class);
});

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::get('transactions/chart', [TransactionController::class, 'chart']);
    Route::apiResource('products', ProductController::class);
    Route::apiResource('authors', AuthorController::class);
    Route::delete('images/{image}', [\App\Http\Controllers\Admin\ImageController::class, 'destroy']);



    Route::post('logout',  [AuthController::class, 'logout']);
    Route::post('refresh',  [AuthController::class, 'refresh']);
    Route::post('me',  [AuthController::class, 'me']);
    Route::delete('users', [UserController::class, 'destroy']);
    Route::patch('promocodes/update_user', [\App\Http\Controllers\PromocodeController::class, 'updateUser']);

    Route::post('users/product_to_cart', [UserController::class, 'storeProductToCart']);
    Route::patch('users/product_to_cart', [UserController::class, 'updateProductToCart']);
    Route::delete('users/product_to_cart',  [UserController::class, 'destroyProductToCart']);

    Route::patch('users', [UserController::class, 'update']);

    Route::post('orders', [\App\Http\Controllers\OrderController::class, 'store']);

    Route::get('orders', [\App\Http\Controllers\OrderController::class, 'index']);
    Route::get('orders/{order}', [\App\Http\Controllers\OrderController::class, 'show']);
    Route::patch('orders/{order}/product', [\App\Http\Controllers\OrderController::class, 'updateProduct']);
    Route::delete('orders/{order}/product', [\App\Http\Controllers\OrderController::class, 'destroyProduct']);
    Route::delete('orders/{order}', [\App\Http\Controllers\OrderController::class, 'destroy']);

    Route::get('transactions', [\App\Http\Controllers\TransactionController::class, 'index']);
    Route::get('transactions/{transaction}', [\App\Http\Controllers\TransactionController::class, 'show']);
    Route::post('transactions', [\App\Http\Controllers\TransactionController::class, 'store']);

    //ORDER transactions
    Route::post('orders/{order}/confirm', [\App\Http\Controllers\OrderController::class, 'confirm']);



});


Route::post('transactions/{transaction}/success', [\App\Http\Controllers\TransactionController::class, 'updateSuccess']);
Route::post('transactions/{transaction}/fail', [\App\Http\Controllers\TransactionController::class, 'updateFail']);



